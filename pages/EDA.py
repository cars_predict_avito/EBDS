import dash
from dash import html

from .templates.kpi import generate_kpi

dash.register_page(__name__, path='/EDA')
import dash
from dash import html
from dash import dcc
import plotly.express as px
import pandas as pd
from dash import dcc, Input, Output, State, callback

# Chargez les données
df = pd.read_csv("C:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\data\\cars_avito.csv", sep=",")


# Dropdown pour le type de carburant
dropdown1 = dcc.Dropdown(
    id='fuel-dropdown',
    options=[{'label': fuel, 'value': fuel} for fuel in df['fuel_type'].unique()],
    value=df['fuel_type'].unique()[0],
    multi=True
)

# Dropdown pour la marque
dropdown_mark = dcc.Dropdown(
    id='mark-dropdown',
    options=[{'label': mark, 'value': mark} for mark in df['mark'].unique()],
    value=df['mark'].unique()[0],
    multi=True
)

# Slider pour le modèle
slider_model = dcc.RangeSlider(
    id='model-slider',
    min=df['year_model'].min(),
    max=df['year_model'].max(),
    value=[df['year_model'].min(), df['year_model'].max()],
    marks={str(year): str(year) for year in range(df['year_model'].min(), df['year_model'].max() + 1, 5)},
    step=1
)


layout = html.Div(children=[
    html.H1(children='Car Data Visualization Dashboard'),
    html.Div(children='''
        Explore car prices and models.
    '''),
    html.Div(
        className='row',
        children=[
            html.Div(
                className='col-4',
                children=[
                    html.Label('Fuel Type'),
                    dropdown1,
                    html.Label('Car Mark'),
                    dropdown_mark,
                    html.Label('Year Model'),
                    slider_model,
                ]
            ),
        ]
    ),
    html.Button(
        id='submit-button-state',
        n_clicks=0,
        children='Submit'
    ),
    html.Div(
        className='row',
        children=[
            html.Div(
                className="col-6",
                children=[
                    dcc.Graph(
                        id='fuel_graph',
                    ),
                ]
            ),
            html.Div(
                className="col-6",
                children=[
                    dcc.Graph(
                        id='price-mark-graph',
                    ),
                ]
            )
        ]
    ),
    html.Div(
        className='row',
        children=[
            html.Div(
                className="col-6",
                children=[
                    dcc.Graph(
                        id='price-model-graph',
                    ),
                ]
            ),
            html.Div(
                className="col-6",
                children=[
                    dcc.Graph(
                        id='year-model-graph',
                    ),
                ]
            )
        ]
    )
    
])

# Graphe Dash pour 'year_model'

@callback(
    Output('year-model-graph', 'figure'),
    [Input('submit-button-state', 'n_clicks')],
    [State('model-slider', 'value')]
)
def update_year_model_graph(n_clicks, year_values):
    filtered_df = df[(df['year_model'] >= year_values[0]) & (df['year_model'] <= year_values[1])]
    fig = px.histogram(filtered_df, x='year_model', nbins=20, color='mark')
    fig.update_xaxes(title_text='Year Model')
    fig.update_yaxes(title_text='Frequency')
    return fig


@callback(
    Output('fuel_graph', 'figure'),
    [Input('submit-button-state', 'n_clicks')],
    [State('fuel-dropdown', 'value')]
)
def fuel_output(n_clicks, fuel_dropdown_value):
    if isinstance(fuel_dropdown_value, str):
        fuel_dropdown_value = [fuel_dropdown_value]
        
    filtered_df = df[df['fuel_type'].isin(fuel_dropdown_value)]
    fig = px.scatter(filtered_df, x='fuel_type', y='price', color='fuel_type')
    return fig

@callback(
    Output('price-mark-graph', 'figure'),
    [Input('submit-button-state', 'n_clicks')],
    [State('mark-dropdown', 'value')]
)   
def update_price_mark_graph(n_clicks, mark_values):
    if isinstance(mark_values, str):
        mark_values = [mark_values]
    
    filtered_df = df[df['mark'].isin(mark_values)]
    print(filtered_df)
    fig = px.strip(filtered_df, x='price', y='mark', title='Price Distribution by Mark')
    return fig

@callback(
    Output('price-model-graph', 'figure'),
    [Input('submit-button-state', 'n_clicks')],
    [State('model-slider', 'value')]
)
def update_price_model_graph(n_clicks, year_values):
    filtered_df = df[(df['year_model'] >= year_values[0]) & (df['year_model'] <= year_values[1])]
    fig = px.scatter(filtered_df, x='price', y='year_model', title='Price Distribution by Year Model')
    return fig

