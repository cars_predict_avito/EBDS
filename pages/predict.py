import os

import numpy as np
import pandas as pd

import dash
from dash import html
from dash import dcc, Input, Output, State, callback
from dash.exceptions import PreventUpdate

import plotly.express as px

import joblib

from .templates.kpi import generate_kpi

dash.register_page(__name__, path='/predict')

# Lisez le fichier CSV en utilisant le chemin relatif
df = pd.read_csv("C:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\data\\cars_avito.csv", sep=",")
fuel_ref = dict(enumerate(df['fuel_type'].unique()))
mark_ref = dict(enumerate(df['mark'].unique()))

# Créez une liste d'options pour le modèle ML
options_model = [
    {'label': model, 'value': model}
    for model in os.listdir('C:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\models\\')  # Mettez le chemin relatif vers le dossier models
]
# Création de listes d'options pour les variables catégorielles
options_fuel_type = [{'label': v, 'value': k} for k, v in fuel_ref.items()]
options_mark = [{'label': v, 'value': k} for k, v in mark_ref.items()]

dropdown1 = dcc.Dropdown(
    id='input-model-filename',
    options=options_model,
    value=options_model[0]['value']
)
dropdown2 = dcc.Dropdown(id='input-feature-3', options=options_fuel_type)  # Dropdown pour fuel_type
dropdown3 = dcc.Dropdown(id='input-feature-4', options=options_mark)  # Dropdown pour mark

slider1 = dcc.Slider(
    id='input-feature-1',
    min=df['year_model'].min(),
    max=df['year_model'].max(),
    value=df['year_model'].mean()
)
slider2 = dcc.Slider(
    id='input-feature-2',
    min=df['mileage'].min(),
    max=df['mileage'].max(),
    value=df['mileage'].mean()
)

slider5 = dcc.Slider(
    id='input-feature-5',
    min=df['fiscal_power'].min(),
    max=df['fiscal_power'].max(),
    value=df['fiscal_power'].mean()
)



layout = html.Div(
    children=[
        html.Div(
            className="row",
            children=[
                html.H1('Car Prices'),  # Correction : "car prices" => "Car Prices"
                html.Div(''),
            ]
        ),
        html.Form(
            className="row g-3 mb-3",
            children=[
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="ML Model"
                        ),
                        dropdown1
                    ],
                ),
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="Year Model"
                        ),
                        slider1
                    ],
                ),
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="Mileage"
                        ),
                        slider2
                    ],
                ),
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="Fuel Type"
                        ),
                        dropdown2
                    ],
                ),
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="Mark"
                        ),
                        dropdown3
                    ],
                ),
                html.Div(
                    className="col-2",
                    children=[
                        html.Label(
                            className="form-label",
                            children="Fiscal Power"
                        ),
                        slider5  # Correction : "slider5" au lieu de "slider3"
                    ],
                ),
                html.Button(
                    id='input-predict-go',
                    className='col-1 btn btn-primary',
                    n_clicks=0,
                    children='Update',
                    type='button'
                )
            ]
        ),
        html.Div(
            id='output-predict-kpis',
            className="row",
        ),
    ]
)


@callback(
    Output(component_id='output-predict-kpis', component_property='children'),
    Input(component_id='input-predict-go', component_property='n_clicks'),
    State(component_id='input-model-filename', component_property='value'),
    State(component_id='input-feature-1', component_property='value'),
    State(component_id='input-feature-2', component_property='value'),
    State(component_id='input-feature-3', component_property='value'),
    State(component_id='input-feature-4', component_property='value'),
    State(component_id='input-feature-5', component_property='value'),
)
def update_graph(n_clicks, model_filename, input_1, input_2, selected_fuel, selected_mark, input_5):
    # Charger le modèle sélectionné
    
    if model_filename == 'cars_reg.joblib':
        model = joblib.load('c:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\models\\cars_reg.joblib')
    elif model_filename == 'cars_KNN.joblib':
        model = joblib.load('c:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\models\\cars_KNN.joblib')
    elif model_filename == 'cars_Descision_Tree.joblib':
        model = joblib.load('c:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\\models\\cars_Descision_Tree.joblib')
    else:
        raise ValueError("Invalid model filename selected")
    
    fuel_bin = np.isin(np.array(list(fuel_ref.keys())), [selected_fuel]).astype(int)
    mark_bin = np.isin(np.array(list(mark_ref.keys())), [selected_mark]).astype(int)
#features = pd.DataFrame([[input_1, input_2, input_3, input_4, input_5]],
#                            columns=['year_model', 'mileage', 'fuel_type', 'mark', 'fiscal_power'])

    # Préparer les données de prédiction
    features = pd.DataFrame([[input_1, input_2, input_5, *fuel_bin, *mark_bin]],  # Créez des colonnes de variables binaires avec des zéros
                            columns=['year_model', 'mileage', 'fiscal_power','fuel_type_Diesel',
                                     'fuel_type_Electrique', 'fuel_type_Essence', 'fuel_type_LPG',
                                     'mark_Acura', 'mark_Alfa Romeo', 'mark_Audi', 'mark_Autres', 'mark_BMW',
                                     'mark_BYD', 'mark_Bentley', 'mark_Cadillac', 'mark_Changhe',
                                     'mark_Chery', 'mark_Chevrolet', 'mark_Chrysler', 'mark_Citroen',
                                     'mark_Dacia', 'mark_Daewoo', 'mark_Daihatsu', 'mark_Dodge', 'mark_Fiat',
                                     'mark_Ford', 'mark_Foton', 'mark_GMC', 'mark_Geely', 'mark_Honda',
                                     'mark_Hummer', 'mark_Hyundai', 'mark_Infiniti', 'mark_Isuzu',
                                     'mark_Jaguar', 'mark_Jeep', 'mark_Kia', 'mark_Land Rover', 'mark_Lexus',
                                     'mark_Maserati', 'mark_Mazda', 'mark_Mercedes-Benz', 'mark_Mitsubishi',
                                     'mark_Nissan', 'mark_Opel', 'mark_Peugeot', 'mark_Pontiac',
                                     'mark_Porsche', 'mark_Renault', 'mark_Rover', 'mark_Seat', 'mark_Skoda',
                                     'mark_Ssangyong', 'mark_Suzuki', 'mark_Toyota', 'mark_UFO',
                                     'mark_Volkswagen', 'mark_Volvo', 'mark_Zotye', 'mark_lancia',
                                     'mark_mini'
                                    ])


    
    y_pred = model.predict(features)[0]
 
    kpi1 = generate_kpi("Estimated Price", f'${y_pred:,.0f}')
    kpi2 = generate_kpi("Year Model", f'{input_1}')
    kpi3 = generate_kpi("Mileage", f'{input_2:,.0f} km')
    kpi4 = generate_kpi("Fuel Type", '{}'.format(fuel_ref[selected_fuel]))
    kpi5 = generate_kpi("Mark", '{}'.format(mark_ref[selected_mark]))
    kpi6 = generate_kpi("Fiscal Power", f'{input_5:,.0f} HP')

    kpis_output = [kpi1, kpi2, kpi3, kpi4, kpi5, kpi6]
    return kpis_output
    