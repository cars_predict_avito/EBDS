import dash
from dash import html

from .templates.kpi import generate_kpi

dash.register_page(__name__, path='/')

layout = html.Div(
    html.Div(
        className="row",
        children=[
            html.H1('Car Price Prediction'),
            html.Div([
                html.P("In this project, I will demonstrate how to predict car prices using various methods, including multiple regression, decision trees, and k-nearest neighbors (KNN). These models will be developed based on multiple features such as mileage, mark, model, model_year, fuel_type, and the city. The data I will work with was extracted from a famous ads platform called "),
                html.A("Avito", href="https://www.avito.ma", target="_blank"),
                html.P("The presentation will be structured as follows:"),
                html.Ul([
                    html.Li("Exploratory Data Analysis."),
                    html.Li("Interactive Prediction.")
                ])
            ]),
        ]
    )
)
