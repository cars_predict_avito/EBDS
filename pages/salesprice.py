import os
import dash
from dash import html, dcc, Input, Output, State, callback
from dash.exceptions import PreventUpdate
import plotly.express as px
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from .templates.kpi import generate_kpi

dash.register_page(__name__, path='/sales-price')

df = pd.read_csv('C:\\Users\\stagiaire\\Desktop\\dashplotly\\cars_predict\data\\cars_avito.csv')

cols = [
    'price',
    'year_model',
    'mileage',   
    'fiscal_power',
]
options = [
    {'label': c, 'value': c}
    for c in cols
]
dropdown1 = dcc.Dropdown(
    id='input-y',
    options=[{'label': 'Sale Price', 'value': 'Price'}],
    value='Price',
    disabled=True
)
dropdown2 = dcc.Dropdown(
    id='input-x',
    options=options,
    value='mileage'
)

layout = html.Div(
    children=[
        html.Div(
            className="row",
            children=[
                html.H1('car prices'),
                html.Div(''),
            ]
        ),
        html.Form(
            className="row g-3 mb-3",
            children=[
                html.Div(
                    className="col-2",
                    children=[dropdown1],
                ),
                html.Div(
                    className="col-2",
                    children=[dropdown2],
                ),
                html.Button(
                    id='input-go',
                    className='col-1 btn btn-primary',
                    n_clicks=0,
                    children='Update',
                    type='button'
                )
            ]
        ),
        html.Div(
            id='output-kpis',
            className="row",
        ),
        html.Div(
            className="row",
            children=[
                html.Div(
                    className="col-8",
                    children=[
                        html.Div(
                            className="card",
                            children=[
                                dcc.Graph(
                                    id='output-graph'
                                )
                            ]
                        )
                    ]
                ),
                html.Div(
                    className="col-4",
                    children=[
                        html.Div(
                            className="card",
                            children=[
                                dcc.Graph(
                                    id='output-graph-hist'
                                )
                            ]
                        )
                    ]
                )
            ]
        )
    ]
)


@callback(
    Output(component_id='output-kpis', component_property='children'),
    Output(component_id='output-graph', component_property='figure'),
    Output(component_id='output-graph-hist', component_property='figure'),
    Input(component_id='input-go', component_property='n_clicks'),
    State(component_id='input-x', component_property='value'),
    State(component_id='input-y', component_property='value')
)

def update_graph(n_clicks, input_x, input_y):
    stats = df[input_x].describe().astype(int)
    kpi1 = generate_kpi("Average ({})".format(input_x), stats.loc['mean'])
    kpi2 = generate_kpi("Stdev ({})".format(input_x), stats.loc['std'])
    graph_ols = px.scatter(df, x=input_x, y='price', trendline="ols")
    graph_hist = px.histogram(df, x=input_x, nbins=50)

    kpis_output = [kpi1, kpi2]
    return kpis_output, graph_ols, graph_hist




