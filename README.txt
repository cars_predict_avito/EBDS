The "amse-myapp" project is composed of two sub-folders and two files:

- folder "src" contains the data science web-app files

- folder "venv" is the virtual environment of the application. To activate the virtual environment, you must execute "venv\scripts\activate" in the CMD prompt, and "venv\scripts\deactivate" to deactivate it. If you use PyCharm, it is possible to associate the virtual environment with the project; PyCharm will then automatically activate and deactivate the environment for you.

- file �requirements.txt� : a text file containing the libraries used in this project. You can pip install them using the command �pip install requirements.txt� in the virtual environment venv

- file �.gitignore� file that enables to exclude (ignore) some files to be tracked by git and also pushed to GitLab. The data science web-app is a multi-page web application.
Lets look at the composition of this folder.

- file main.py is the file that is executed to launch the application, using the command "python app.py".

Once the app is launched, the user can change pages by clicking on the buttons in the navigation bar. Those buttons are associated with URLs and depending on the requested page, app.py will execute the right Python file.

- folder �assets� : will load the css files
- folder �data� : contains the data
- folder �models� : contains trained models 
- folder �pages� contains three pages : home, EDA, predict and carprice, as well as useful functions to avoid redundancy in code.
- file __init__.py thanks to the __init__.py file, Python will look for submodules inside the "data-app" directory, and enable to import modules inside it.